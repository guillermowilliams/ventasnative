/*
MySQL Backup
Source Server Version: 5.5.5
Source Database: ventas
Date: 2/04/2019 06:44:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `boleta`
-- ----------------------------
DROP TABLE IF EXISTS `boleta`;
CREATE TABLE `boleta` (
  `numBol` varchar(8) NOT NULL,
  `fechaBol` datetime DEFAULT NULL,
  `estadoBol` varchar(1) DEFAULT NULL,
  `codCli` varchar(5) NOT NULL,
  `codVen` varchar(5) NOT NULL,
  PRIMARY KEY (`numBol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `cliente`
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `codCli` varchar(5) NOT NULL,
  `nombreCli` varchar(40) DEFAULT NULL,
  `direCli` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codCli`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `detallebol`
-- ----------------------------
DROP TABLE IF EXISTS `detallebol`;
CREATE TABLE `detallebol` (
  `numBol` varchar(8) NOT NULL,
  `codPro` varchar(8) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `puPro` decimal(10,0) DEFAULT NULL,
  `importe` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `producto`
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `codPro` varchar(8) NOT NULL,
  `nombrePro` varchar(20) DEFAULT NULL,
  `puPro` decimal(10,0) DEFAULT NULL,
  `stockPro` float DEFAULT NULL,
  PRIMARY KEY (`codPro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `vendedor`
-- ----------------------------
DROP TABLE IF EXISTS `vendedor`;
CREATE TABLE `vendedor` (
  `codVen` varchar(5) NOT NULL,
  `nombreVen` varchar(40) DEFAULT NULL,
  `direVen` varchar(50) DEFAULT NULL,
  `dniVen` varchar(8) DEFAULT NULL,
  `sexoVen` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`codVen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `cliente` VALUES ('','Maria','Copelo');
INSERT INTO `vendedor` VALUES ('1','Juan','Asociacion de vivienda Raucana','74446252','M');
