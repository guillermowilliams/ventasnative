import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class Menu extends Component {
    constructor(props){
        super(props);
        this.state = {
            dataOptions:[
                {key:'Clientes'},
                {key:'Vendedores'},
                {key:'Productos'},
                {key:'Boleta'},
                {key:'Detalle boleta'},
            ]
        }
    }
    static navigationOptions = {
        title: 'Dashboard',  
    }
    GetGridViewItem(item) {
        this.props.navigation.navigate('CrudOptions')
    }
    render() {
        return (
            <View style={styles.container}>
                <FlatList
                data={ this.state.dataOptions }
                renderItem={ ({item},key) =>
                    <View key={key} style={styles.GridViewContainer} >
                        <Text style={styles.GridViewTextLayout} onPress={this.GetGridViewItem.bind(this, item.key)}> {item.key} </Text>
                    </View> }
                numColumns={2}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#e5e5e5",
        paddingTop: 10,
    },
    headerText: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
        fontWeight: "bold"
    },
    GridViewContainer: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
        margin: 5,
        backgroundColor: '#007bff',
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    GridViewTextLayout: {
        fontSize: 20,
        fontWeight: 'bold',
        justifyContent: 'center',
        color: '#fff',
        padding: 10,
    }
});