import Login from './Login'
import Menu from './Menu'
import CrudOptions from './CrudOptions'
import {createStackNavigator, createAppContainer} from 'react-navigation'

const AppNavigator = createStackNavigator({
    //Screens   
    Login: {
        screen: Login
    },
    Menu: {
        screen: Menu
    },
    CrudOptions: {
        screen: CrudOptions
    },
}, {
    //settings
    initialRouteName: 'Login'
})
export default createAppContainer(AppNavigator)
