import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native'
import { List, ListItem } from "react-native-elements"
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class CrudOptions extends Component {
    static navigationOptions = {
        title: 'Opciones',  
    }
    constructor(props){
        super(props);
        this.state = {
            myData:[
                {
                    slug:'insertar',
                    title: 'Insertar Cliente',
                    icon: 'add-circle'
                },
                {
                    slug:'editar',
                    title: 'Editar Cliente',
                    icon: 'create'
                },
                {
                    slug:'eliminar',
                    title: 'Eliminar Cliente',
                    icon: 'delete'
                },
                {
                    slug:'consultar',
                    title: 'Consultar Cliente',
                    icon: 'search'
                },
            ]
        }
    }
    goMantain = (slug) =>{
        alert(slug)
    }
    render() {
        return (
            <View style={styles.container}>
                {
                    this.state.myData.map((item, i) => (
                        <ListItem
                            key={i}
                            title={item.title}
                            leftIcon={{ name: item.icon }}
                            onPress={() => this.goMantain(item.slug)}
                        />
                    ))
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#e5e5e5",
    },
});