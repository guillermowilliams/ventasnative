import React, { Component } from 'react'
import {ToastAndroid} from 'react-native';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard 
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email:'',
            password:'',
            validacion:false
        }
    }
  static navigationOptions = {
    header: null,    
  }
  Logear = () =>{
    if (this.state.email == '' && this.state.password == '') {  
        ToastAndroid.show('Complete los campos!', ToastAndroid.SHORT);
    }else{
        if (this.state.email == 'guillermojuica3@gmail.com' && this.state.password == '123') {
            this.props.navigation.navigate('Menu')
            ToastAndroid.show('Bienvenido al sistema!', ToastAndroid.SHORT);    
        }else{
            ToastAndroid.show('Usuario o contraseña invalidos!', ToastAndroid.SHORT);
        }
    }
  }
  render() {
    const Divider = (props) => {
        return <View {...props}>
          <View style={styles.line}></View>
          <Text style={styles.textOR}></Text>
          <View style={styles.line}></View>
        </View>
    }
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}> 
          <View style={styles.up}>
            <Ionicons
              name="ios-cart"
              size={100}
              color={'#007bff'}>
            </Ionicons>
            <Text style={styles.title}>
              Sistema de ventas
          </Text>
          </View>
          <View style={styles.down}>
            <View style={styles.textInputContainer}>
              <TextInput
                style={styles.textInput}
                textContentType='emailAddress'
                keyboardType='email-address'
                placeholder="Ingresa tu email"
                onChangeText={email => this.setState({email})}
              >
              </TextInput>
            </View>
            <View style={styles.textInputContainer}>
              <TextInput
                style={styles.textInput}
                placeholder="Ingresa contraseña"
                secureTextEntry={true}
                onChangeText={password => this.setState({password})}
              >
              </TextInput>
            </View>
            <Divider style={styles.divider}></Divider>
            <TouchableOpacity onPress={this.Logear} style={styles.loginButton}>
              <Text style={styles.loginButtonTitle}>Ingresar</Text>
            </TouchableOpacity>
          </View>
        </View> 
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',  
    backgroundColor: '#fff'
  },
  up: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
    down: {
    marginTop:30,
    flex: 3,//70% of column
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  title: {
    color: '#212529',
    textAlign: 'center',
    width: 400,
    fontSize: 23
  },
  textInputContainer: {
    paddingHorizontal: 10,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#ced4da',
    borderRadius: 50,
    marginBottom: 20,
    backgroundColor: 'rgba(255,255,255,0.2)'//a = alpha = opacity
  },
  textInput: {
    width: 300,
    height: 45
  },
  loginButton: {
    marginTop: 15,
    width: 280,
    height: 45,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#007bff'
  },
  loginButtonTitle: {
    fontSize: 18,
    color: 'white'
  },
  facebookButton: {
    width: 300,
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  line: {
    height: 1,
    flex: 2,
    backgroundColor: 'black'
  },
  textOR: {
    flex: 1,
    textAlign: 'center'
  },
  divider: {
    flexDirection: 'row',
    height: 40,
    width: 298,
    justifyContent: 'center',
    alignItems: 'center'
  }
})